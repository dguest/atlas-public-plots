DICE latch structure, based on the conventional cross coupled inverter latch
 structure where the four nodes X1 to X4 store data as two pairs of complementary
 values. The original cell state is restored when an SEU error is introduced
in a single node.

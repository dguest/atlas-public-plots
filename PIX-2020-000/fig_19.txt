Fraction of pixels in which TDAC-bit2 flips, measured at the beginning of LHC fill 6371 and after about \SI{150}{\per\pico\barn} of integrated luminosity for 3D modules and after the end of LHC fill for planar modules with) $0\rightarrow1$.


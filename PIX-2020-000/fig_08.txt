Schematic view of the IBL readout system. From left to right: the detector, the BOC, the ROD and the calibration farm. All
detector readout fibers are located at the back of the crate (BOC). New user-pluggable connections (GBit Ethernet) are on the front
side (ROD).

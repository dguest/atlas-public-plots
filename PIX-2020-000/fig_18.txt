(a) Image of the flex cable with L-shaped HitOr line attached
to the sensor.
(b) The map (columns versus rows) of L-shaped noise in the reworked FBK-type 
 3D IBL module LI-S14-C-M4-C8-2 accumulated by random triggers during empty bunches of LHC fill 6239 with integrated luminosity \SI{0.13}{\per\femto\barn}.


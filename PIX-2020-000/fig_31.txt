Standard deviation of the average ToT per pixel after noise subtraction
versus integrated luminosity in fill 7018 from 2018, shown in the eight rings of 3D modules.

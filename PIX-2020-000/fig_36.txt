The probability for a latch to undergo a $0\rightarrow1$ or $1\rightarrow0$ transition measured for the output enable bit for FDAC-MSB for FE-I4B in each planar module from negative-$z$ side to positive-$z$ side.
 The $P_{0\rightarrow1}$ is not present for the output enable bit because almost all pixels in FE-I4B have this bit set to 1, and a measurement of such transition is not possible for this latch.


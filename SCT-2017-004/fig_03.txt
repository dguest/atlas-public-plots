Comparison between data (points) and Hamburg/Dortmund model predictions 
(lines with uncertainties shown by the coloured bands) of the leakage 
current per unit volume at 0oC (I vol) of the four barrel layers. The 
integrated luminosity and beam energy are also shown. Sensor 
temperatures are shown indicating periods of normal operation as well as 
the extended periods with no beam in the LHC when the SCT was off. This 
plot supersedes a previously approved plot, and simply extends the date 
range to the end of 2017. 

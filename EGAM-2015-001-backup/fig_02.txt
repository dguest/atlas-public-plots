Invariant mass of diphoton candidates in 13 TeV Monte Carlo normalised to the same number
of entries as data for 50 < m(gamma-gamma)< 500 MeV.
The photon candidates are required to fulfill Et>1.5 GeV, |eta|<2.47 excluding 1.37<|eta|<1.52
and to deposit more than 10% of their energy in the first layer of the
electromagnetic calorimeter.

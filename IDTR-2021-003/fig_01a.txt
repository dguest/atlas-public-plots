(a) Inclusive track reconstruction efficiency for displaced charged particles
produced by the decay of long-lived scalar particles, a. The efficiency is
shown as a function of the radius of production of displaced particles and the
absolute value of the transverse impact parameter, $|d_{0}|$. The efficiency is
defined by the ratio of the number of signal truth particles matched to
reconstructed tracks divided by the number of signal truth particles. Truth
particles are subject to fiducial selections as detailed in
[<a href="https://cds.cern.ch/record/2275635">ATL-PHYS-PUB-2017-014</a>]. 

 Fraction of inactive modules for each component of the ATLAS pixel detector at the end of Run 1,
 during a long shut down period just after Run1 and at the beginning of Run 2. The module with less hit
 occupancy by 10% with respect to the average value in the layer is defined as inactive for the Run 1 and the
 Run 2. Estimate of inactive modules after LS1 Re-installation is based on tests that are less reliable in
 identifying modules with open HV. This mainly justifies the small differences between this and the Run 2
 data-taking based accounting. Two different sensor technologies (planar and 3D) are used for the IBL which
 was installed during LS1. The 3D sensors are located outside of the tracking acceptance.

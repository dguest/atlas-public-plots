<TITLE>
Luminosity plots for the 2017 &#8730;s=13 TeV high-luminosity running period
<DATE>
14-05-2018
<INTRO>
These plots correspond to the preliminary analysis of the 2017 &#8730;s=13 TeV high-luminosity running period, released in Spring 2018. Figures 1 and 2 show details of the vdM scan to establish the absolute luminosity scale. Figures 3 and 4 show the non-factorisation analysis of the vdM scan data. Figures 5-7 show studies of bunch-train effects in the relative normalisation of the LUCID and track-counting luminosity measurements. Figure 8 illustrates the assessment of systematic uncertainties on the luminosity calibration transfer between the vdM scan and physics regimes, by comparing the luminosity estimates from the Tile calorimeter and track counting measurements in the vdM fill and a subsequent physics fill. Figures 9-12 show the relative long-term stability of various luminosity estimates throughout the 2017 data-taking period. Figures 13 and 14 illustrate the parameterisation of the LUCID &mu;-dependence by comparison to track-counting
data in two reference fills taken in July and September 2017.

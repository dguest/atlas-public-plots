Observed invariant mass spectrum of the selected diphoton candidates (black dots). Superimposed is the SM prediction (blue histogram). 
The main contribution is irreducible: production of pairs of isolated photons via QCD processes. 
The comparatively small contribution from events with at least one jet being misidentified as a photon is also shown
separately (green histogram). The reducible component is estimated using data control samples. The shape of the 
$m_{\gamma \gamma}$ distribution of the irreducible component is determined using simulation, and it is normalised to the data (after 
subtraction of the reducible component).
Both photons need to satisfy $E_{\rm T} > 55$ GeV, tight photon identification and be isolated in the calorimeter. The highest-mass 
event has $m_{\gamma \gamma} = 940$ GeV. The number of expected events with $m_{\gamma \gamma} > 940$ GeV is $1.1$ .

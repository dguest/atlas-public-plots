#!/usr/bin/env python

from shutil import copy2, copytree, move
import os

d = "."
content = os.listdir(d)

for o in content:
    path = os.path.join(d,o)
    if os.path.isdir(path):
        if "-" in path and "backup" not in path:
            print ("path = %s" % path)
            try:
                copytree(path, path + "-mcolautt")
                move(path, path + "-backup")
                move(path + "-mcolautt", path)
            except:
                print ("can't proceed correctly on folder % s " % path)


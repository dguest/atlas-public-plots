<TITLE>
Vertex reconstruction performance in high pile-up conditions
<DATE>
11-10-2016
<INTRO>
One of the challenges posed by the HL-LHC conditions will be to preserve the ability to efficiently reconstruct the primary vertex corresponding to the hard-scattering process and discriminate it from pile-up interactions, which are expected to reach an average of $\langle\mu\rangle=200$ per bunch-crossing. The following figures extrapolate the performance of the current algorithm (see IDTR-2016-003) to high pile-up conditions. They rely on a simulation of the ATLAS detector that includes the upgraded inner tracker currently in development (ITk), with its fully inclined layout. Please see ATL-PHYS-PUB-2016-025 (Expected Performance of the ATLAS Inner Tracker at the High-Luminosity LHC), for further details about the ITk layouts and software used to produce these plots.
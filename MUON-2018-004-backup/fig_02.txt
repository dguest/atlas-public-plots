Reconstruction efficiency for Medium muons close to jets with the selection topoetcone20/$p_T$(probe) > 0.06 .
Where the variable topoetcone20 is defined in Eur. Phys. J. C (2016) 76:292.
The resulting values are plotted as distinct measurements in each bin with $p_T$ increasing from 5 to 15 GeV, going from left to right.
The total reconstruction efficiency is measured as the product between the Muon Spectrometer efficiency and the Inner Detector efficiency.

Muon reconstruction efficiencies for the Medium identification algorithm measured
in $Z\rightarrow\mu\mu$ and $J/\psi\rightarrow\mu\mu$ events as a function of
the muon momentum. The prediction by the detector simulation is depicted as empty
circles (squares), while the full circles (squares) indicate the observation in
collision data for $Z\rightarrow\mu\mu$ ($J/\psi\rightarrow\mu\mu$) events.
Only statistical errors are shown in the top panel. The bottom panel reports the
efficiency scale factors. The darker error bands indicate the statistical uncertainty,
while the lighter bands indicate the quadratic sum of statistical and systematic
uncertainties. In two bins ($12<p_T<20\,GeV$), results are given for both
$Z\rightarrow\mu\mu$ and $J/\psi\rightarrow\mu\mu$ events. 


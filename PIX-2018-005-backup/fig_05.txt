The simulated E-field profile inside an IBL sensor for 150 V using the Chiochia radiation damage model [1] implemented in the Silvaco [2] TCAD software.  

[1] V. Chiochia et al., A Double junction model of irradiated silicon pixel sensors for LHC, Nucl. Instrum. Meth. A568 (2006) 51. 

[2] Silvaco, http://www.silvaco.com, Santa Clara, California (USA). 
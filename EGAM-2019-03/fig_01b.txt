Electron identification efficiency and scale factors as a function $E_{\mathrm{T}}$, for electrons with 2.5 < $|\eta|$ < 4.9.
Forward electrons are reconstructed using a topological clustering algorithm, and identified using a likelihood-based discriminant.
The efficiencies are measured double differentially in 5 bins in $|\eta|$ and 4 bins in $E_{\mathrm{T}}$,
and shown in data and simulation for Loose, Medium and Tight operating points.
The efficiencies are measured using $Z \rightarrow$ ee events with one central ($|\eta|$ < 2.47) and one forward electron.
The contributions from background are estimated using a template fit to the electron pair invariant mass distribution.
The scale factors are obtained from the ratio of efficiencies measured in data and simulation.

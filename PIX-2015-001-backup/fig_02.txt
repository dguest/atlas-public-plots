Distribution of Time Over Threshold (ToT) Corrected to Normal
Incidence for the Pixel Barrel Clusters


Cluster on-track ToT distributions for the three
pixel barrel layers, during the recent 13 TeV
run and one of the last p-p runs of 2012; ToT is
corrected to normal incidence.

Distributions are normalized to unity.


The distribution reproduces fairly well the
measured one in December 2012. The slight
observed shift is under investigation.

The vertex position resolution in 2016 data, corrected with resolution correction factors. The
correction factors are the ratio of the vertex position resolution calculated with the Split-Vertex
Method and the inherent vertex position error from the vertex reconstruction fit.
Shown versus the number of tracks per vertex for the x direction. 

Electron identification efficiencies in $Z\rightarrow ee$ events as a function of 
transverse energy $E_\mathrm{T}$, integrated over the full pseudo-rapidity range. 
The efficiencies have been measured in 8.8 $\mathrm{fb}^{-1}$ of data recorded by 
the ATLAS experiment in 2016 at a centre-of-mass energy of $\sqrt{s}$ $=$ 13 TeV, 
and are compared to MC simulation. The total statistical and systematic uncertainty 
is displayed. The lower efficiency in data than in MC arises from the fact that the 
MC does not properly represent the 2016 TRT conditions, in addition to the known 
mismodelling of calorimeter shower shapes in the GEANT4 detector simulation. 
Both of these differences between data and MC were considered when optimising the 
likelihood-based selection criteria for 2016 data. The background subtraction for 
the efficiency measurement was performed using the $Z_\mathrm{mass}$ method, which 
is described along with the electron identification algorithms and the other efficiency 
measurement techniques in ATLAS-CONF-2016-024.

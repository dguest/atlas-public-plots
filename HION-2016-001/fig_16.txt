The uncorrected charged-particle spectra are shown as a function of pT. The red crosses depict spectrum obtain only by min-bias trigger itself. The black points show spectrum when tracks from min-bias trigger and all jet triggers are combined.


Distribution of hit residuals, defined as the distance of the i-th hit in a
muon track segment fitted with a straight line where the i-th hit is excluded
form the fit.
The data sample is from the main physics stream for 10 runs taken 
between Oct 27 and Nov 2 2015 at the center of mass energy of 13 TeV.
Included are 1162 out of 1174 MDT chambers, each with at least 1,000 hits.
A segment is required to be part of a muon at $p_{T}>20 $ GeV and to have at 
least 8 (6) hits for a 8 (6) tube-layer MDT chamber.
The $R(t)$ function is provided by the gas monitor chamber valid for the
day of data collection.
One time chamber-specific $T_{0}$ and $R(t)$ corrections are applied.
The reported residual width derives from a Gaussian distribution function
fit to the FWHM.

Distribution of the number of pixel hits associated to selected particle tracks in 13.6 TeV pp collisions in data
(filled points with error bars) and simulation (continuous line).

Average d<sub>0</sub> correction to compensate bias as a function of &eta; and &phi; for the partial 2016 proton-proton dataset from the beginning of 2016 data-taking until Technical Shutdown 1.

Average d<sub>0</sub> correction to compensate bias as a function of &eta; and &phi; for the partial 2016 proton-proton dataset from Technical Shutdown 1 until July 21.

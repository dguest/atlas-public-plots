Efficiency for B layer hits associated to a reconstructed particle
  track in 13 TeV collision events as a function of the track pseudo rapidity
  $|\eta|$ in Run 2.
  The B layer efficiency is determined by computing the ratio of selected
  tracks with an associated pixel hit in the B layer to the total number of
  selected tracks that are extrapolated to a B layer module. Tracks are
  requested to have at least a total of 9 (11) hits in the Pixel and SCT
  layers for $|\eta| \le$ 1.65 ($>$ 1.65), hits in the IBL and Layer 1 pixel
  sensors, nominal track-vertex-association criteria, $|d_{0}| < $ 2 mm and
  $|\Delta z_0 \sin \theta| <$ 3 mm where $d_0$ is the transverse impact
  parameter with respect to the beam line, $\Delta z_0$ the difference between
  the longitudinal impact parameter of the track and the primary vertex when
  expressed at the beam line and $\theta$ the polar angle of the track, and
  have transverse momentum in excess of 2 GeV. 
  The lines give the efficiency measured during the 2015 (continuous line),
  2016 (dashed line) and at the end of the 2017 (dotted line) runs, after
  1.5, 6.5 and 89.3 fb$^{-1}$ of delivered integrated luminosity in Run~2 and
  with an average number of pile-up events of 13, 22, 2 and 29, respectively.
  The observed efficiency decrease with time in the central region is
  due to the reduction of collection efficiency as a result of radiation damage.
  The markers give the efficiency measured at the beginning of the 2018 run
  with different configurations for the analog threshold and the
  Time-over-threshold (ToT) cut. The configuration named
  ``Hybrid Analog Threshold'' uses a cut at
  4300~$e^-$s for the four modules located at the center of the detector and
  at 5000~$e^-$s for the more external modules. This configuration optimises
  the response on the central modules, where charged particles have a shorter
  path in the active thickness of the sensors and leave less charge, with a
  reduced bandwidth usage on the modules located further away from the center
  and is adopted for the 2018 data taking.

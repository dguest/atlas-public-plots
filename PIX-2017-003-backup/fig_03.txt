Fit results for the Lorentz angle &theta;<sub>L</sub> in each of the runs investigated, together with the accumulated luminosity for each run. The uncertainties on the Lorentz angle are statistical only, and have been scaled to
$\sqrt{ \chi^{2} /N_{dof}}$ 
 for those fits where $\chi^{2} > N_{dof}$.

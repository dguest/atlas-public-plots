The variation in on-detector IPIN currents as a function of time during Run2, for the four barrel layers. The IPIN current reflects the intensity of the incoming TX optical signal.
Each trend line is normalised to <I_PIN>, which is the arithmetic mean of non-zero IPIN values across the date range, and is shown for each barrel layer in the legend.
The increase in IPINs for barrel layers 3,4 and 5 during 2015 were due to a temporary increase in operating temperatures (barrel layer 6 always runs at elevated temperature). The gap in the centre of the plot corresponds to when the detector was powered off during the end of year shutdown.


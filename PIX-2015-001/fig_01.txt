Cluster on-track ToT distribution for the two IBL sensor
technologies: Planar and 3D sensors. ToT is corrected to normal
incidence. 


Threshold was 2650 electrons, setup temperature was -10° C. The
bias voltage was 80 and 20 V for planar and 3D sensors respectively.


Tracks were selected if they had at least 15, 6 and 2 hits in the TRT,
SCT and Pixel (including IBL) respectively.


Good quality on-track clusters were selected if they are away from
the sensor active edge by more than 1.5.


Distributions are normalised to unity.


The different Landau peak position is due to the thickness
difference between the two sensors: 0.200 mm for Planar and
0.230 mm for 3D sensors.

Clusters Occupancy for Pixel Barrel Layers in December
2012 and May 2015: Layer 1 (Dec 2012). 

These plots show the number of clusters for each Pixel module. Modules
are mapped following their stave number (azimutal position, vertical axis)
and their position within the stave (z position, horizontal axis).

Modules who had no cluster are shown in white: they are dead modules.

We can see a large improvement with respect to the end of Run 1, thanks
to the nSQP installation and the consequent reparing efforts.

Dead modules went from 10, 10 and 47 to 4, 8 and 16 for B-layer, Layer 1
and Layer 2 respectively.

Several new dead modules can be observed in the present map. Most of
them had connection failures in bias voltage, low voltage or data path.

Dead PP0 (7 modules) in layer 2 at stave index = 18 is not permanent and
not counted in the given numbers of dead modules. It resulted in an
interlock after failure of a temperature readout.
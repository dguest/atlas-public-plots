Efficiency of the isolation requirement FixedCutLoose for electrons from $Z\to ee$ in data and MC as a function of $p_{\mathrm{T}}$ (a) and $\eta$ (b).
The isolation requirement is defined using two discriminant variables: the calorimeter isolation energy, $E_{\mathrm{T}}^{\mathrm{cone}0.2}$,
is the sum of transverse energies of the topological clusters in a cone $\Delta R < 0.2$ around the electron candidate, excluding the contribution due to
the electron cluster and corrected for the contribution from the electron energy leakage and for the pileup contribution using an ambient energy density correction;
and the track isolation, $p_{\mathrm{T}}^{\mathrm{varcone}0.2}$, is the sum of transverse momenta of all tracks, satisfying quality requirements,
in a cone $\Delta R = \min(0.2, 10\ \mathrm{GeV} / E_{\mathrm{T}})$ around the candidate electron track and originating from the reconstructed primary vertex of the hard collision,
excluding the electron associated tracks. The definition of the isolation requirement is described in ATLAS-CONF-2016-024.
The electrons are required to fulfill the Medium selection from the likelihood based electron identification. The efficiencies have been measured using data
recorded by the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13\ \mathrm{TeV}$.

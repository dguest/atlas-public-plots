Efficiency of the isolation requirement FixedCutTight and FixedCutHighMuTight for electrons
from $Z\to ee$ in data as a function of the number of interactions per bunch crossing, $\mu$.
The ratio between the efficiency in data and the efficiency in MC is presented in the bottom panel.
The FixedCutHighMuTight isolation requirement was designed to be more pile-up robust and slightly
more efficient than the FixedCutTight one, defined in ATLAS-CONF-2016-024. It is defined using two discriminant variables:
the calorimeter isolation energy, $E_{\mathrm{T}}^{\mathrm{cone}0.2}$, is the sum of transverse energies of
the topological clusters in a cone $\Delta R < 0.2$ around the electron candidate, excluding the contribution
due to the electron cluster and corrected for the contribution from the electron energy leakage and for the
pileup contribution using an ambient energy density correction; and the track isolation, $p_{\mathrm{T}}^{\mathrm{varcone,tight} 0.3}$,
is the sum of transverse momenta of all tracks, satisfying quality requirements, in a cone
$\Delta R = \min(0.3, 10\ \mathrm{GeV} / E_{\mathrm{T}})$ around the candidate electron track and originating
from or used to define the reconstructed primary vertex of the hard collision, excluding the electron associated
tracks. The requirement in the two variables is then $E_{\mathrm{T}}^{\mathrm{cone}0.2}/p_{\mathrm{T}} < 0.15$ and
$p_{\mathrm{T}}^{\mathrm{varcone,tight} 0.3}/p_{\mathrm{T}} < 0.04$.
The electrons are required to fulfill the Medium selection from the likelihood based electron identification.
The efficiencies have been measured using data recorded by the ATLAS experiment in 2017 at a centre-of-mass
energy of $\sqrt{s} = 13\ \mathrm{TeV}$.

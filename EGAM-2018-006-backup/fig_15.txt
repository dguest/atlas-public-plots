Efficiency of the isolation requirement FixedCutTight (defined in ATLAS-CONF-2016-024) for electrons from $Z\to ee$ in data as a function
of $p_{\mathrm{T}}$, comparing two different ranges of the number of interactions per bunch crossing $\mu<40$ and $\mu>40$.
The ratio between the efficiency in data and the efficiency in MC is presented in the bottom panel.
The electrons are required to fulfill the Medium selection from the likelihood based electron identification. The efficiencies have been measured using data
recorded by the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13\ \mathrm{TeV}$.

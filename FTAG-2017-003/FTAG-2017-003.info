<TITLE>
b-tagging efficiency calibration from 2015+2016 data on lepton+jets t<span style="text-decoration: overline">t</span> events
<DATE>
29-06-2017
<INTRO>
The efficiency of the MV2c10 b-tagging algorithm has been measured using lepton+jets t<span style="text-decoration: overline">t</span> events produced in pp collisions at &#8730;s=13 TeV collected by the ATLAS detector in 2015 and 2016. The b-tagging  efficiencies and the corresponding data-to-simulation scale factors are measured as a function of the transverse momentum and pseudo-rapidity of the jets and also as a function of quantities that are sensitive to close-by jet activity. The results extend the regions in which it is possible to extract data-to-simulation b-tagging scale factors, compared to b-tagging calibrations based on dilepton t<span style="text-decoration: overline">t</span> events.

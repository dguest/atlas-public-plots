The trend of detector control statuses of IBL modules of one run (270953) for (a) 
a stably operated stave (Stave02), and (b) a particular stave (Stave11) in which the low 
voltage power supply was interlocked in the middle for module group M4C (corresponding 
to modules C7 and C8, and each module group contains four FE chips). The categories in 
the colour scale are the finite state machine states of the IBL detector control system.

The current consumption of a single FE-I4b chip in data taking condition as a function of the
total ionizing dose in linear (a) and logarithmic (b) x-axis scale.

The plots are Testbeam results regarding the two planar quad KEK (HPK) sensors Q9 (unirradiated) and Q16 (irradiated: 4.31e15 neq/cm$^2$).<br>

The properties of the investigated quad sensors are: <br>
The KEKQ9 with HPK pre-production sensor was tested in November 2022. The sensor is unirradiated and was tuned to 2ke. It has a thickness of 150 $\mu$m. The applied bias voltages are: 20V, 40V, 60V, 80V, 100V, 120V. <br>
The KEKQ16 was tested in September 2023. The sensor was irradiated with proton to a fluence of 4.31e15 neq/cm$^2$  at CYRIC. 
It was tuned to 1ke and has a thickness of 150 $\mu$m . The effective bias voltages are 396V, 495V, 600V. <br>

All sensors were tested in perpendicular position. No additional masks for the DUTs besides the ones used during the tesbeam were applied. 
A matching radius of (100 $\mu$m, 100 $\mu$m) was used for the track reconstruction and the cluster association of the DUTs.  
Dead pixels were masked during reconstruction.


Q9 Cluster Map (bias voltage: 100V) <br>
Cluster maps of the unirradiated planar quad sensors KEKQ9 (based on 4680651 clusters) at 100V effective bias. 
The pixels of the interchip region register a larger number of hits due to their larger area. 
The beam spot is cut off, due to particles being outside the area covered by the pairs of scintillators.


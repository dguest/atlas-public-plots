Data to simulation comparison for the transverse momentum ($p_{\rm T}$)
scale (top) and resolution (bottom) as a function of pseudorapidity ($\eta$) 
obtained from $Z\rightarrow \mu^{+}\mu^{-}$ candidates. The dataset consists 
of 6.6 pb$^{-1}$ of proton--proton collision data collected in
2015 by the ATLAS detector at a centre-of-mass energy of $\sqrt{s}=13\ \rm
TeV$ and compared with $Z$ simulation. 
Muons are reconstructed combining  Inner Detector and Muon Spectrometer tracks.  
Each event must contain two opposite-charge muons with $20 \ {\rm GeV} < p_{{\rm T}} < 55 \ {\rm GeV}$ and $|
\eta| <2.5$. The invariant mass of the dimuon system ($m_{\mu^{+}\mu^{-}}$) must
be in the range of $75 \ {\rm GeV} < m_{\mu^{+}\mu^{-}} < 105 \ {\rm GeV}$.
The binning is defined according to the $\eta $ of the leading muon. 
A fit to data and simulation 
of a Crystal-Ball (CB) probability density function (pdf) allows for a comparison to
simulations post $p_{\rm T}$-calibration. The mean of the
CB (top) is used as an estimator of the $p_{{\rm T}}$-scale dependence for data and
simulations, respectively.  The width of the CB (bottom) is used as an estimator
of the detector resolution for both data and simulations. Systematic
uncertainties (filled area) are derived from simulations by
shifting to $\pm 1~\sigma$ the smearing and scale parameters, used to derive
the correction. 
A 3\% uncertainty due to the miss modeling of the resolution from the CB is also included in the systematics.

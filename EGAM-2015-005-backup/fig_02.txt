Electron identification efficiency in $Z\rightarrow ee$ events as a function of pseudo-rapidity $\eta$ , integrated over transverse energy $E_{\rm{T}}$.
The efficiency is shown for three operating points that are based on a likelihood approach, Loose, Medium and Tight. 
The efficiencies have been measured in 85 $\rm{pb}^{-1}$ of data recorded by the ATLAS experiment in 2015  at a centre of mass energy of $\sqrt{s}=13$ TeV. 
They are compared to efficiencies in MC simulation. 
The total statistical and systematic uncertainty is displayed. 
The lower efficiency in data than in MC mostly arises from a known mismodelling of calorimetric shower shapes in the GEANT detector simulation, which has been accounted for when the selection criteria were optimized for run-2 data taking. 
The data-MC differences are similar to what was observed for the 2010-2012 data taking period. 
Both likelihood based electron identification and the measurement of efficiencies are described in ATLAS-CONF-2014-032. 
The likelihood based electron identification has been reoptimised for 2015 data taking conditions.

A typical eye-diagram for a 10G-link where an lpGBT-V1 drives a VTRX+ to generate the link signal. 
With an optical sampling scope, bandwidth 7.5GHz, many points from many traces are overlaid and 
the required mask, EDMS document 1719329,  from the CERN-ASIC-team is shown in grey. Figure shows a typical eye-diagram for a 10G-link where an lpGBT-V1 drives a VTRX+.
Ratio, per B, of the number of reconstructed tracks with a wrongly assigned B-layer hit and the total number of reconstructed tracks for 4 different reconstruction strategies.

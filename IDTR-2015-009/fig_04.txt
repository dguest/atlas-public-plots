Unfolded transverse impact parameter resolution, $\sigma(d_{0})$, measured from data for one run taken in September (black) 2015 with the Inner Detector as a function of track $p_{T}$ for $0.0 < \eta < 0.25$, compared to that measured from data for one run taken in November(red) 2015 when significant IBL bowing had been observed due to increase of temperature at IBL originating from total ionization dose effects.
The data used for this study are reconstructed with corrected alignment constant.
The highest number of interaction per bunch crossing for the two runs is around 16.
The resolution is not significantly worse with IBL distortion.

Event display of a high-multiplicity  p+Pb collision at $\sqrt{s_\mathrm{NN}} = 5.02$ TeV with 319 reconstructed tracks. All tracks 
are from a single vertex and have $p_\mathrm{T}>0.4$ GeV.

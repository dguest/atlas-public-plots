Number of reconstructed charged particles $N_{\mathrm ch}^{\mathrm{sel}}$ versus total transverse energy deposited in the forward 
calorimeter in Pb-going (left, $\Sigma E_\mathrm{T}^{\mathrm{Pb}}$) and $p$-going (right, $\Sigma E_\mathrm{T}^{\mathrm{p}}$) sides, 
covering the pseudorapidity $3.1<|\eta|<4.9$. The 
energy is measured at the electromagnetic scale. Events are required to have a reconstructed vertex. Tracks are selected from a 
pseudorapidity range $|\eta|<2.5$ with transverse momentum $p_\mathrm{T}>0.1$ GeV.

Distribution of signal amplitudes in the Pb-going Zero-Degree Calorimeter (ZDC), positioned 140m from the ATLAS interaction point, 
from 5.02 TeV p+Pb collisions, taken in November 2016. The ZDC is primarily sensitive to forward-going neutral particles, in 
particular neutrons, and contributions from one, two, three, and four neutrons are clearly visible.

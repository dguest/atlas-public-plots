Dependence of the ratio $R$ of bunch-by-bunch instantaneous luminosities 
measured 
by track counting and by the LUCID HitOR algorithm, on the position of the
bunch within a bunch train, counting zero as the start of the train.
The dependence of the luminosity ratio on the pileup parameter $\mu$ was
measured using a `$\mu$-scan' procedure, where the LHC beams were partially
separated in order to reduce the instantaneous luminosity
(and hence the number of inelastic interactions per bunch crossing $\mu$)
step-by-step. The dependence of the ratio on $\mu$ was parameterised 
as $R=p_0+p_1\mu$, and the quantity plotted is the slope $p_1$ corresponding
to the rate of change of $R$ with increasing pileup. LHC fill 6194 had two 
48-bunch trains with 25\,ns spacing and two 56-bunch `8b4e' trains with a 
repeating pattern of 8 bunches separated by 25\,ns followed by 4 empty 
bunch crossings, and three isolated bunches.
The results for both train types are shown, together with those for the
isolated bunches at the right-hand side of the plot. The uncertainties are
statistical.

Fractional difference in run-integrated luminosity between the LUCID HitOR 
algorithm, and the TILE, EMEC, FCal, TPX and track-counting algorithms, as a 
function of the cumulated delivered luminosity (normalized to the total 
delivered in 2017). From the beginning of 2017 data-taking until luminosity 
fraction 0.32 (resp. between luminosity fraction 0.32 to the end of 2017 
data-taking) the LUCID luminosity has been subjected to a pile-up-dependent 
correction, using as a reference the track-based luminosity measured in a 
reference run corresponding to a luminosity fraction of about 0.19 
(resp. 0.58). In the period from the beginning of 2017 data-taking to 
luminosity fraction 0.03, the LUCID luminosity has been subjected to an 
additional, smaller correction based on comparisons with the tracking, 
TILE and EMEC luminosity algorithms. The small discontinuity around luminosity 
fraction 0.32 corresponds to the boundary between the two pile-up-dependent 
correction periods. Each point corresponds to an ATLAS run recorded during 
25ns bunch-train running in 2017 at &#8730;s=13 TeV. 
The luminosity measurements by TILE, EMEC, FCal, TPX and Tracking have been 
normalized to LUCID in the reference physics run marked by the red arrow.

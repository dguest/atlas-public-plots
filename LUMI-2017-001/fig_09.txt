Fractional difference in run-integrated luminosity between the LUCID HitOR 
algorithm, and the TILE, EMEC, FCal, TPX, track-counting and Z-counting
algorithms. 
The LUCID luminosity in the period from the beginning of 2017 data-taking to 
August 12th (resp. from August 14th to the end of 2017 data-taking) has been 
subjected to a pile-up-dependent correction, using as a reference the 
track-based luminosity measured in a reference run recorded on July 29th 
(resp. September 30th). In the period from June 5th to June 18th, the 
LUCID luminosity has been subjected to an additional, smaller correction 
based on comparisons with the tracking, TILE and EMEC luminosity algorithms. 
The small discontinuity around August 15th corresponds to the boundary between 
the two pile-up-dependent correction periods. Each point corresponds to an 
ATLAS run recorded during 25ns bunch-train running in 2017 at &#8730;s=13 TeV.
The luminosity measurements by TILE, EMEC, FCal, TPX, Tracking and Z-counting
have been normalized to LUCID in the reference physics run indicated by the red
 arrow. Z-counting/LUCID points only show runs with a Z-counting statistical
error below 0.5% (no statistical uncertainties are plotted).

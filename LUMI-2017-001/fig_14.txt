Ratio of the bunch-averaged pile-up parameter &lt;&mu;<sub>Tracking</sub>&gt; 
reported by the track-counting luminosity algorithm, to the 
&lt;&mu;<sub>Algo</sub>&gt; value obtained using either the LUCID HitOR or the 
TILE algorithms, as a function of &lt;&mu,sub>Algo</sub>&gt; during reference 
LHC Fill 6259. In this figure the run-integrated luminosity provided by the 
TILE algorithm has been normalized to that from track counting in Fill 6024.
The &mu;-dependence of the LUCID response measured in this fill (red squares) 
is parameterized by a straight-line fit (solid red line), which is used to 
correct the LUCID luminosity on a minute-by-minute basis from LHC Fill 6079 
(August 14th) until the end of 2017 data-taking. This correction amounts 
to approximately -0.15% per unit of &mu;.

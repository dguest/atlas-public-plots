Performance gain for jets in $t\overline{t}$ events at the 60% WP in
$c$-/light-flavour rejection of different DIPS models in comparison to the
recommended RNNIP.

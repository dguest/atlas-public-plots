Efficiency of the FixedCutLoose muon isolation working point, as a function of the muon transverse
momentum, measured in $Z\rightarrow\mu\mu$ events. The black dots indicate the efficiency measured in data while red 
circles indicate the prediction from simulation. The errors on the efficiency are statistical. The bottom panel shows the 
ratio between data and simulation, as well as the statistical uncertainties (purple) and combination of statistical and 
systematic uncertainties (yellow). Backgrounds are estimated via a template-fit to data.

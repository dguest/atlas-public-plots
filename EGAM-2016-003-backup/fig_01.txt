Comparison of the radiative Z boson data-driven tight identification efficiency
measurements of unconverted photons to the Z to ll$\gamma$ simulation as
function of  $E_\mathrm{T}$ of the photon in the region 15 <  $E_\mathrm{T}$ < 100 GeV, for the entire
pseudorapidity region (excluding the transition region between the barrel and
end-cap calorimeters) . The bottom panel shows the ratio of the
data-driven results to the MC predictions (scale factor, SF). Only
the statistical uncertainties are shown. The background contamination
in the $E_\mathrm{T}$  > 15 GeV region is estimated to be less than 1% [1] and
therefore is neglected.

[1] ATL-PHYS-PUB-2016-015   http://cds.cern.ch/record/2203514


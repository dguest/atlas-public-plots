<html>
<head>
<title>fig_06</title>

</head>

<body>

	Cumulative signal acceptance times efficiency as a function of the G<sup>*</sup><sub>KK</sub> &rarr; HH resonance mass for Higgs candidates tagged using the D<sub>Xbb</sub> tagger <a href="https://cds.cern.ch/record/2724739">[ATL-PHYS-PUB-2020-019]</a> and the DL1r algorithm evaluated on variable radius jets. The events are required to have at least two double-b-tagged large-R jets with p<sub>T</sub> &gt; 250 GeV, |&eta;| &lt; 2.0, and mass preselection requirements. The jets from H &rarr; b<span style="text-decoration:overline">b</span>  decay originate from samples of Randall-Sundrum Gravitons (G<sup>*</sup><sub>KK</sub>) that decay into two Higgs bosons, each of which subsequently decays into a pair of bottom quarks. The H &rarr; b<span style="text-decoration:overline">b</span> tagging efficiency for the leading large-R jet (p<sub>T</sub> &gt; 450 GeV) in these Graviton MC samples increases between 20% and 110% (for masses between 1 TeV and 6 TeV) when using the X &rarr; b<span style="text-decoration:overline">b</span> tagger compared to tagging 2 variable-radius (VR) track-jets using the DL1r algorithm. For comparison, the increase in H &rarr; b<span style="text-decoration:overline">b</span> tagging efficiency in a Pythia8 multijet MC sample is of 40% for the leading large-R jet. However, when selecting 2 H &rarr; b<span style="text-decoration:overline">b</span>-tagged large-R jets (p<sub>T</sub> &gt; 250 and p<sub>T</sub> &gt; 450 GeV), the acceptance is increased by 50-350% in the Graviton samples for masses between 1 TeV and 6 TeV, compared to 100% in the multijet MC.

</body>
</html>

Average d<sub>0</sub> correction to compensate bias as a function of &eta; and &phi; for the partial 2016 proton-proton dataset from July 21 until the end of the 2016 <i>pp</i> data-taking.

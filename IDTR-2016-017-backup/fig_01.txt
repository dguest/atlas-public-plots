Average correction to compensate the bias of the track sagitta as a function of &eta; and &phi; for the full 2016 proton-proton dataset.

The average fraction of modules with synchronization errors for each run for IBL (black), B-Layer (red), Layer1 (green) and Layer2 (blue) in Run2.
The fraction of synchronization error of IBL decreases by more than two orders of magnitude due to improvement of the ROD firmware.

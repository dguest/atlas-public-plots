Sagitta bias in the 2016 data as a function of $\eta$ and $\phi$ for the prompt alignment (left) and the reprocessed alignment (right) using the $E/p$ method.

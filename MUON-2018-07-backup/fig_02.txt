Muon reconstruction efficiencies for the Loose/Medium/Tight identification algorithms measured
in $Z\to\mu\mu$ events as a function of the muon pseudorapidity for muons with $p_{\textrm{T}} > 10$ GeV. The prediction
by the detector simulation is depicted as open circles, while filled dots indicate the observation in collision data with statistical uncertainties. 
The bottom panel shows the ratio between expected and observed efficiencies, the efficiency scale factor. 
The errors in the bottom panel show the quadratic sum of statistical and systematic uncertainties. The results are 
based on the full $pp$ collision data collected in 2018 at 13 TeV.

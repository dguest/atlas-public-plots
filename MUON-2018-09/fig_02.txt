Gap currents as a function of instantaneous luminosity for gas gaps in station 1 in barrel middle/outer large/small (BML/BMS, BOL/BOS) RPC chambers in side A and $\phi$ sector 1$/$2 of the ATLAS detector. Currents are averaged over gas gaps in each station, normalized to gas gap areas and corrected for calibration currents. Corresponding linear fits are displayed for each chamber category.


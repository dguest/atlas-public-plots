Single pixel efficiency maps for a fully depleted non-irradiated sensor. The sensor shows a high efficiency of ~100%, except in the corner areas (70-85%), visible as the blue cells. These inefficiencies are due to the p+ etched columns, and are expected in perpendicularly mounted 3D sensors. 

Note that perpendicular tracks in the sensor is a worst case scenario. In the final detector, most tracks will hit the sensor at an angle, increasing efficiency in the electrode regions.

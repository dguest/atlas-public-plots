Comparison of the average electron <i>p</i><sub>T</sub> between 2023 (markers) and 2018 (solid histogram) lead-lead data from ultra-peripheral collisions.
The <i>&gamma;</i><i>&gamma;</i> &rarr; <i>e</i><sup>+</sup><i>e</i><sup>-</sup> event selection is imposed on the data which requires exactly two reconstructed electrons with opposite-electric charges and acoplanarity below 0.02. 
The 2018 data is normalised to a number of events in the 2023 set with electron &#9001;<i>p</i><sub>T</sub><sup style='position: relative; left: -0.4em;'>reco</sup>&#9002;&gt; 7 GeV.
The excess of events at low&#9001;<i>p</i><sub>T</sub><sup style='position: relative; left: -0.4em;'>reco</sup>&#9002;in the 2023 data originates from the dedicated optimisation of egamma reconstruction.

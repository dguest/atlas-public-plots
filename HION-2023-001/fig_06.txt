Average underlying event subtraction for jets in Pb+Pb collisions at &radic;s<sub>NN</sub> = 5.36 TeV in the pseudorapidity interval |&eta;| &lt; 1.0. The results are reported in different selections of event activity, measured as the total transverse energy in the forward calorimeters (FCal).
The jets were reconstructed using the anti-k<sub>t</sub> algorithm with <i>R</i>=0.4.

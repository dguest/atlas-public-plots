The subtracted transverse momentum (<i>p</i><sub>T, subtracted</sub>) as a function of the sum of the
transverse energy deposited in the forward calorimeter (FCal &Sigma; <i>E</i><sub>T</sub>) in Pb+Pb
collisions at &radic;s<sub>NN</sub> = 5.36 TeV in the psuedorapidity interval |&eta;|&lt;1.0. The jets
were reconstructed using the anti-k<sub>t</sub> algorithm with <i>R</i>=0.4. No attempt has been made to remove
events containing pile-up.

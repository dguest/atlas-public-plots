Leading jet transverse momentum spectra for central (0.0 &lt; |&eta;| &lt; 3.2) and forward (3.2 &lt; |&eta;| &lt; 4.9) jets in Pb+Pb collisions at &radic;s<sub>NN</sub> = 5.36 TeV.
The jets are reconstructed using the anti-k<sub>t</sub> algorithm with <i>R</i>=0.4.

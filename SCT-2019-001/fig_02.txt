The figures show the distribution of the leakage current per unit volume normalized to 0 degree C at HV of 150V for all the SCT end-cap modules as of Nov. 1, 2018. Modules of sides C and A and those with HPK (Hamamatsu) and CiS sensors are shown in different colours. High-eta modules (disks 8 and 9) draw more current than those in low-eta regions, a trend indicated by the FLUKA simulation.


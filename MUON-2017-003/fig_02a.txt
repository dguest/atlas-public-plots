Average number of MDT and CSC precision hits in the full detector (a), and in the innermost
layer of the MS (b) per reconstructed track for muons from Z → µµ satisfying the Medium working point,
as a function of the pseudorapidity for muons with p_{T} > 10 GeV. The same scenarios as in Figure 1
are shown.

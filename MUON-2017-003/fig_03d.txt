Fraction of muons reconstructed in Z->µµ events satisfying the
Medium working point but not originating from the Z boson decay in the region 0.1 < |η| < 1.3 before (a and c) and after (b and d) applying additional track-to-vertex association
requirements. The same scenarios as in Figure 1 are shown. The bottom
panel compares the results to the <µ> = 0 running scenario.

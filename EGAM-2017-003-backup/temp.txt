Electron reconstruction and identification efficiencies in $Z \rightarrow ee$ events as a function of
transverse energy ET, integrated over the full pseudo-rapidity range, for electrons 
with ET > 4.5 GeV. The efficiencies are shown in data and
MC for three operating points that are based on a likelihood approach, Loose, Medium and Tight.
The data effciencies are obtained by applying data/MC efficiency ratios that were measured in
$J/\psi \rightarrow ee$ and $Z \rightarrow ee$ events to MC simulation. \A dataset corresponding to an integrated
luminosity of 33.9 fb-11 that was recorded by the ATLAS experiment in the year 2016 at a
centre-of-mass energy of $\sqrt{s}$ = 13 TeV was used. The total statistical and systematic uncertainty
is displayed. The lower efficiency in data than in MC arises from the fact that the MC does not
properly represent the 2016 TRT conditions, in addition to the known mismodelling of calorimeter
shower shapes in the GEANT4 detector simulation. Both of these differences between data and
MC were considered when optimising the likelihood-based selection criteria for 2016 data. Both
the likelihood based electron identification algorithms and the measurement of efficiencies are
described in ATLAS-CONF-2016-024.


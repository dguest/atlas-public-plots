<TITLE>
Pixel Charge Collection Efficiency
<DATE>
14-08-2023
<INTRO>
The operation of the ATLAS Pixel detector in Run 3 is characterised by large particle fluxes inducing significant radiation damage effects in the detectors, in particular those located closest to the LHC interaction region. The innermost layer of the ATLAS Pixel detector (IBL) [1] has already sustained an average fluence in excess of 10e15n-eq/cm [2].  In order to model the effects of this particle fluence on the detector response, ATLAS has developed a radiation damage simulation based on realistic maps of the electric field in the Si bulk at difference values of fluences and operating bias voltages, now implemented in the official simulation software, adopted for Run 3 ATLAS Monte Carlo (MC) physics sample simulation [3]. 
The present set of plots tracks the radiation damage effects on the charge collection efficiency for the two innermost layers, IBL and B-layer planar and IBL 3D sensors, as a function of the integrated luminosity using data from 2015 to 2023, comparing them to radiation damage simulation. Predictions for the evolution of the charge collection efficiency in 2024 and 2025 are also given together with the evolution of the pixel cluster most probable value of the charge for IBL 3D sensors as a function of the applied detector bias voltage from early 2016 to the end of 2022. 
References
[1] ATLAS Collaboration, ATLAS Insertable B-Layer: Technical Design Report, ATLAS-TDR-19; CERN-LHCC-2010-013, 2010, url: https://cds.cern.ch/record/
1291633; Addendum: ATLAS-TDR-19-ADD-1; CERN-LHCC-2012-009, 2012, url: https://cds.cern.ch/record/1451888.; B. Abbott et al., Production and
Integration of the ATLAS Insertable B-Layer, JINST 13 (2018) T05008
[2] ATLAS Collaboration, Measurements of sensor radiation damage in the ATLAS inner detector using leakage currents, JINST 16 (2021) P08025, arXiv:
2106.09287 [hep-ex].
[3] ATLAS Collaboration, Modelling radiation damage to pixel sensors in the ATLAS detector, JINST 14 (2019) P060
[4] A. Folkestad et al, Development of a silicon bulk radiation damage model for Sentaurus TCAD, Nucl. Instr. and Meth., 874 (2017), 94-102.

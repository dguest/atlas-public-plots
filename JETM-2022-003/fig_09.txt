 Number of Particle Flow Objects
 Number of particle flow objects (PFOs) reconstructed as gathered by data quality monitoring in June
 2022 when stable beams of protons at the injection energy of 450 GeV per beam were delivered to
 ATLAS during the LHC commissioning.

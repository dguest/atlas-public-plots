Electromagnetic(EM) Scale TopoCluster Jet Rapidity
Rapidity of jets reconstructed at the EM scale as anti-$k_t$, $R = 0.4$ Calorimeter Topological
Cluster (TopoCluster) jets as gathered by data quality monitoring in June 2022 when stable beams
of protons at the injection energy of 450 GeV per beam were delivered to ATLAS during the LHC
commissioning.
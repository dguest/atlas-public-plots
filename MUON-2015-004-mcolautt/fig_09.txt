
   Muon reconstruction efficiencies for the $Medium$ identification algorithm measured in 
   $J/\psi\to\mu^+\mu^-$  and  $Z\rightarrow\mu^{+}\mu^{-}$ events as a
   function of the muon momentum. The prediction by the detector
      simulation is depicted as empty circles (squares), while the full circles (squares) 
      indicate the observation in collision data for $Z\rightarrow\mu^{+}\mu^{-}$
      ($J/\psi\to\mu^+\mu^-$) events. 
N.B.: only statistical errors are shown in the top panel.  
      The bottom panel reports the 
      efficiency scale factors. The darker error bands indicate the
      statistical uncertainty, while the lighter bands indicate the quadratic sum of
      statistical and systematic uncertainties.  
      In three bins (10 GeV $< p_{T} <$ 20 GeV), results are given for both $Z\rightarrow\mu^{+}\mu^{-}$ and $J/\psi\to\mu^+\mu^-$ events.





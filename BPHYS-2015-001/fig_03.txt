The transverse momentum p<sub>T</sub> distribution of muons from candidate J/&psi;&rarr;&mu;<sup>+</sup>&mu;<sup>-</sup> decays observed in &radic;s = 13 TeV pp collisions.

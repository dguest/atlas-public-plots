Efficiency of the isolation requirement $E_{\text T, iso}(\text{calo})/E_{\text T}$<0.2 and $E_{\text T, iso}(\text{tracks})/E_{T}$<0.15 
for electrons from $Z \to ee$ as a function of the transverse energy $E_{\text T}$ , for 0.1<$\eta$ < 0.6.
$E_{\text T, iso}(\text{calo})$ is the sum of the transverse energies of the clusters of calorimeter energy deposits 
in a cone $\Delta R (=\sqrt{\Delta\eta^{2}+\Delta\phi^{2}})$ < 0.2 around the electron candidate, excluding the contribution
due to the electron cluster and
corrected for the contribution from the electron energy leakage and for the pileup contribution using an ambient energy density correction.
$E_{\text T, iso}(\text{tracks})$ is the sum of the transverse momenta of the tracks in a cone $\Delta R = min(0.2,10 \text{ GeV}/ E_{\text T})$,
excluding the track associated to the electron candidate.
The electrons are required to fulfill the tight selection from the likelihood based electron identification. 
The efficiencies have been measured in 3.2 fb$^{-1}$ of data recorded by the ATLAS experiment in 2015 at a centre-of-mass energy of $\sqrt{s} = 13$ TeV.


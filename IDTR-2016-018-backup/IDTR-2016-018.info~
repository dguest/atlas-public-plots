<TITLE>
Impact Parameter Resolution Using 2016 MB Data
<DATE>
13-12-2016
<INTRO>
The intrinsic impact parameter resolution of tracks in the transverse, $sigma;(d_0)$, and longitudinal, $sigma;(z_0)$, directions are measured in minimum-bias simulation and in 2016 data. The impact parameter resolution is extracted in fine bins of $p_T$ and $\eta$ of the tracks using iterative Gaussian fits and the impact parameter resolution of the primary vertex, $sigma;(d^{vtx}_0)$ or  $sigma;(z^{vtx}_0)$, is deconvoluted from the measured total resolution, $sigma;(d^{tot}_0)$ or $sigma;(z^{tot}_0)$, using an iterative unfolding method described in <a href="https://cds.cern.ch/record/1281352">ATLAS-CONF-2010-070</a> Systematic uncertainties of the method, shown in one-dimension projections plots, take into account: primary vertex resolution uncertainty, unfolding procedure, non-Gaussian tails of the resolution fit, data-taking period dependence assessed by comparing events collected in 2015 and in 2016 runs.

The jet yield in pp collisions at $\sqrt {s} =$ 5.02 TeV as a function of $p_T$ in the $|\eta| < 2.1$ range. Events match the 
HLT_j75_L1J20 trigger. The statistical uncertainties are indicated by error bars.

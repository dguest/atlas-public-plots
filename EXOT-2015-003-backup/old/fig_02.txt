Event display of the highest-mass diphoton event. Event information: Run and event numbers = 270806, 28091225. 
Invariant mass $m_{\gamma \gamma} = 940$ GeV, transverse momentum of the diphoton system = $318$ GeV.
Leading photon $E_{\rm T}$, $\eta$, $\phi$, $E_{\rm T}^{\rm iso}$ = $374$ GeV, $1.71$, $-0.97$, $1.54$ GeV. 
Subleading photon $E_{\rm T}$, $\eta$, $\phi$, $E_{\rm T}^{\rm iso}$ = $212$ GeV, $-0.60$, $1.15$, $-0.08$ GeV. 
The leading photon is converted. The isolation energy $E_{\rm T}^{\rm iso}$ is defined as the sum of the transverse energies of cells 
inside topo-clusters inside a cone of $\Delta R = 0.4$ around the photon candidate, excluding a 5×7 grid of EM cells in the
centre of the cone.

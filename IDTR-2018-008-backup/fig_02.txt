The intrinsic transverse impact parameter resolution $\sigma_{z0}$ for
TightPrimary tracks associated to jets with $p_T > 20$ GeV measured in di-jet
triggered pp collisions data collected in 2017 (Red) and 2018 (Blue). The
trasnverse impact parameter resolution is extracted in bins of pT of tracks
using iterative Gaussian fits.

The evolution of the mean and RMS of the equivalent noise charge (ENC) over all pixels in the IBL detector as a function of the integrated luminosity and the corresponding total ionizing dose (TID) in 2015, as measured in calibration scans. The detector was regularly retuned. Each color/symbol series corresponds to a single tuning of the detector.



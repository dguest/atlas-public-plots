Distributions of the calibrated energy, $E_{calib}$, divided by the
generated energy, $E_{gen}$, for a Monte Carlo sample of unconverted
photons generated with $1.52 < |\eta| < 1.81$ and $50 < E_T^{gen} <
100$ GeV, without pileup. The dashed-line (red) histogram shows the
performance when the photons are reconstructed and calibrated using
the current clustering algorithm (so-called sliding window, used in
release 20.7 up to 2016 data taking) while the full-line (blue)
histogram is based on the new clustering algorithm (so-called
super-clusters, used in release 21 starting from 2017 data
taking). The curves represent Gaussian fits to the cores of the
distributions, between $[-1, +2]$ standard deviations, and provide the
results shown for the observed resolutions $\sigma$.

<TITLE>
Jet core tracking pileup dependence
<DATE>
25-09-2018
<INTRO>
The following plot shows how the ratio of the summed charged particle track transverse momentum in a jet core to the jet transverse momentum depends upon pileup in 2018. It is compared with the same quantity extracted in Pythia and Sherpa dijet simulations. The plot uses anti-k$_{t}$ 0.4 jets with p$_{T}$ over 450 GeV. Tracks which pass the tight primary selection, and are aligned with the jet direction to better than 0.1 in $\Delta$R are selected. The scalar sum of their transverse momentum is computed and divided by the jet p$_{T}$. Use of the narrow jet core ensures that tracks have the dense environment which CTIDE studies. It also suppresses contribution from additional pileup tracks to a negligible level. Taking the ratio to the jet p$_{T}$ strongly reduces dependence on modelling the transverse momentum distribution.
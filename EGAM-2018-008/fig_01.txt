Inclusive di-electron invariant mass distribution from $Z \to ee$ decays in data compared to MC after applying 
the full calibration. No subtraction of the background (expected to be at the level of 0.5% and with a non-peaking 
m$_{ee}$ distribution) is applied, and the simulation is normalised to data. The lower panel shows the data to
simulation ratio, together with the uncertainty from the energy scale and resolution corrections. These
uncertainties are estimated following the same procedure as discussed in https://inspirehep.net/record/1306905
with one modification consisting in varying the mass window used to obtain the energy scale corrections in a
wider range to probe the impact of non gaussian tails in the energy response. They were estimated mainly from 2016
data. 

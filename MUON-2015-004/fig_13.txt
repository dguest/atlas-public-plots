Transverse momentum ($p_{\rm T}$) scale as a function of pseudorapidity ($\eta$) obtained from reconstructed 
$J/\psi\rightarrow \mu^{+}\mu^{-}$ candidates.  The dataset, consisting of 3.3 fb$^{-1}$ of proton--proton collision data collected in 2015
by the ATLAS detector at a centre-of-mass energy of $\sqrt{s} = 13 \ {\rm TeV}$ (filled markers), 
is compared to prompt $J/\psi$ simulation (continuous line).  
Muons are reconstructed combining  Inner Detector and Muon Spectrometer tracks. 
 Each event must contain two opposite-charge muons with $5 \ {\rm GeV} < p_{\rm T} < 17 \ {\rm GeV}$ and $|\eta| <2.5$. The invariant mass of the dimuon system ($m_{\mu^{+}\mu^{-}}$) must be in the range
of $2.6 \ {\rm GeV} < m_{\mu^{+}\mu^{-}} < 3.6 \ {\rm GeV}$. 
The binning is defined according to the $\eta $ of the leading muon. 
A fit to data and simulation of a Crystal-Ball (CB) probability density function (pdf) plus a Gaussian pdf modeling secondary detector smearing,  plus an exponential pdf modeling the background in data allows for a
comparison of the width of CB, used as an estimator of the detector resolution, to
simulations post $p_{\rm T}$-calibration (labeled as MC). Systematic uncertainties (filled area)
are derived from $\pm 1\sigma$ variations of the smearing and scale parameters used to
derive the correction. 
An uncertainty accounting for the missmodeling  of the fitting function due to the missing background component is also taken into account.

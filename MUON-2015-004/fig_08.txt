 Muon reconstruction efficiencies of the muon track-to-vertex association requirements measured in $Z\rightarrow\mu^+\mu^-$ events as 
a function of the muon pseudorapidity. The prediction by the detector simulation is depicted as open circles, while filled dots indicate the observation in 
collision data. The bottom panel shows the ratio between expected and observed efficiencies, the efficiency scale factor. Green error bands indicate the 
statistical uncertainty, and orange bands the quadratic sum of statistical and systematic uncertainty.



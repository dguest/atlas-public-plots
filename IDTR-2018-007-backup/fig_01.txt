The fraction of the transverse momentum of anti-k<sub>t</sub> 0.4 jets with p<sub>T</sub>
over 450 GeV which is carried by charged particle tracks within 0.1 in ∆R of
the jet direction. This is displayed for di-jet triggered pp collision data collected
in 2018 (black), and with the hard process simulated in Pythia (green) and
Sherpa (red). In each case the pileup is simulated using Pythia. The
simulations are normalised to the same mean as the data. All the results are
stable to 5% or better up to the highest pileup densities

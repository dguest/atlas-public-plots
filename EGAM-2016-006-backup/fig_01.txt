Invariant mass distribution of opposite sign electron-positron pairs for low and high &mu; 
events in 2016 data. Electrons or positrons are required to satisfy E<sub>T</sub> > 27 GeV, |&eta;|&lt;2.47, 
and the medium likelihood identification. The energy scale factors obtained from 2015 
data are applied. The shift between low and high &mu; events is less than one per mil 
(percent) on the mean (resolution) of the distribution.

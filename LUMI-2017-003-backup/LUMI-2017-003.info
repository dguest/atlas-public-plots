<TITLE>
Z-counting luminosity plots for the 2017 &#8730;s=13 TeV high-luminosity running period
<DATE>
29-05-2018
<INTRO>
In 2017, the LHC delivered instantaneous luminosities of up to 2 10<sup>34</sup>cm<sup>−2</sup>s<sup>−1</sup> at &#8730;s=13 TeV. This permitted monitoring of the luminosity using the counts of reconstructed Z&rarr;&mu;&mu; events selecting two muons with p<sub>T</sub>>27GeV, |&eta;| < 2.4 and 66 < m<sub>&mu;&mu;</sub>< 116 GeV with a time granularity as short as 60s. The Z counting rate was corrected using trigger and reconstruction efficiencies determined in situ including a Monte Carlo correction for residual effects. The statistical uncertainty of the luminosity determination from the Z&rarr; &mu;&mu; counting rate is better than 0.5 &percnt; for 80 &percnt; of the 185 ATLAS runs which comprise 97 &percnt; of the Z-count event statistics. The plots show preliminary results illustrating the basic principle of the counting of Z bosons as a luminometer, including the stability over the full 2017 &#8730;s=13 TeV data taking period.

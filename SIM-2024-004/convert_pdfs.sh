#!/bin/bash
#
# Marilena Bandieramonte marilena.bandieramonte@cern.ch
# 08/03/2024
# Script to convert the .pdf images into .png, rename them and automatically
# Create the public plot page

if [ $# -ne 1 ]; then
  echo "Usage: $0 <folder>"
  exit 1
fi

folder=$1

# Check if the folder exists
if [ ! -d "$folder" ]; then
  echo "Error: Folder '$folder' not found."
  exit 1
fi

# Iterate through all PDF files in the folder
for pdf_file in "$folder"/*.pdf; do
  if [ -f "$pdf_file" ]; then
    # Extract the file name without extension
    base_name=$(basename "$pdf_file" .pdf)
    
    # Convert PDF to PNG using pdftoppm
    pdftoppm -png "$pdf_file" "$folder/$base_name"
    # Rename the converted PNG file
    mv "$folder/$base_name-1.png" "$folder/$base_name.png"
    
    echo "Converted and renamed: $pdf_file"
  
  fi
done

# Run the Python script with the folder name as the ID
python3 ~atlaspo/public/scripts/createPages.py --PLOTS "$folder"
echo "Python script executed with ID: $folder"
echo "Conversion, renaming, and Python script execution complete."
    echo "Converted: $pdf_file"


The distribution
of the mass of secondary vertices reconstructed by the SV algorithm,
applied to jets in a ttbar-dominated 
sample of events selected by  requiring an opposite-sign e-mu pair and 
at least two jets. The data is shown by the points with error bars 
and the simulation by  the filled areas, divided into contributions from 
b (red), c (green) and light (purple) flavoured jets, and normalised
to the same number of secondary vertices as the data.

The table shows the slope of the occupancy per μ obtained from the fitting of
the plot and average occupancy at μ=50 in each layer. Presented hit occupancy
was determined by subtracting the noise occupancy, measured in the events
collected by a PixelNoise stream, from the observed occupancy in the events
collected by a physics_ZeroBias stream in 2018 in separate LHC fills.  

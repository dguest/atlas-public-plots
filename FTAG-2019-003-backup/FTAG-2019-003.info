<TITLE>
ATLAS Flavor-Tagging Calibration Results with 80.5 fb${}^{-1}$
<DATE>
12-03-2019
<INTRO>
A variety of algorithms have been developed to distinguish $b$-quark jets ($b$-jets) from jets containing only lighter quarks within the ATLAS experiment at the Large Hadron Collider (LHC). The tagging efficiencies of $b$-jets and the mis-tag rates of $c$-jets and light-jets of two widely used algorithms, MV2 and DL1, are measured in $pp$ collision data collected with the ATLAS detector during 2015, 2016 and 2017 at $\sqrt{s} = 13$ TeV, corresponding to an integrated luminosity of 80.5 fb${}^{-1}$, for both the anti-$k_{t}$ $R=0.4$ EMTopo Calorimeter jets and anti-$k_{t}$ Variable-$R$ jets. The results are compared with those in MC to extract Data/MC efficiency corrections ("scale factors") applied to the MC after a smoothing procedure [JHEP 08 (2018) 89]. The following plots summarize the scale factors derived for $70\%$ single cut operation point.

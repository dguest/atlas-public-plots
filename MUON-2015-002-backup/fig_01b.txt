Muon reconstruction efficiency of the \emph{Medium} working point for 
$ p_T > $ 10 GeV  
measured using $Z\to\mu^+ \mu^-$ decays in uniform $\eta-\phi$ regions of the Muon Spectrometer (top) and as a function of the muon transverse momentum (bottom). 
 Black dots indicate the efficiency measured in data while red circles indicate the prediction by the simulation. 
 The errors on the efficiencies are statistical. 
 The bottom panel shows the ratio between the result in data and simulation, with statistical (green) and the combination of statistical and systematic uncertainties (orange). 
 Systematic uncertainties include the charge-dependency of the efficiencies, uncertainties on the residual background from multijet events, and possible efficiency biases introduced by the tag\&probe method.

The average hit readout occupancy, i.e. the average number of hits per pixel per event, for IBL(black), BLayer(red), Layer-1(blue), Layer-2(green), Disks(purple) 
as a function of the average number of interactions per bunch crossing ($\mu$) for events collected by a zero-bias trigger in 2016. Each point corresponds to an average over
four LHC fills(5339, 5393, 5394, and 5416). The solid lines show the linear interpolation for each layer up to $\mu\sim80$.
The fit results of the occupancy($10^{-5}$ hits/pixel/event) by linear function are $(2.021 \pm 0.002)\mu - (0.160 \pm 0.050)$ for IBL, 
$(1.316 \pm 0.001)\mu - (0.049 \pm 0.032)$ for B-Layer, $(0.7378 \pm 0.0007)\mu - (0.007 \pm 0.017)$ for Layer-1, 
$(0.4650 \pm 0.0005)\mu - (0.674 \pm 0.012)$ for Layer-2, $(0.6489 \pm 0.0007)\mu - (0.140 \mu 0.015)$ for Disks.

This figure shows the uncorrected(*) hit efficiencies as a function of HV for four typical modules from Barrels 3 to 6 as of October 2018. Hit efficiency gets lower as HV is lowered. The closer to the beam pipe, the lower the efficiency at low HV region as expected by radiation damage. The Barrel 3 curve indicates the VFD (full depletion voltage) may be at around 100V.
(*)The track selection criteria imposes a small bias on calculated hit efficiency at low values of HV, which is not corrected for in this figure.


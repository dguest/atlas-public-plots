Scanning grid for the generalised, two-dimensional vdM scan performed
during LHC fill 8381 (10 - 11 Nov 2022). The axes indicate the
horizontal ($\Delta x$) and vertical ($\Delta y$) nominal separation
between the beams, in units of the nominal single-beam width $\sigma$.
Luminosity data were recorded for 15 s or 30 s per scan step at small
(orange squares) or large (blue circles) beam separation respectively,
to improve the statistical precision in the tails of the scan.


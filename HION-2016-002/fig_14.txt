Event display of a p+Pb collision at $\sqrt{s_\mathrm{NN}} = 8.16$ TeV containing a photon and multiple jets. The photon passes tight 
identification and isolation criteria ($E_\mathrm{T}^{\mathrm{iso,}R\mathrm{=0.3}} < 3$ GeV).

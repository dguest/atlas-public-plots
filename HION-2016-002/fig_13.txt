Event display of a p+Pb collision at $\sqrt{s_\mathrm{NN}} = 8.16$ TeV containing a di-photon candidate. The photons pass tight 
identification criteria and are experimentally isolated ($E_\mathrm{T}^{\mathrm{iso,}R\mathrm{=0.3}} < 3$ GeV), and are balanced in 
azimuth and transverse momentum.

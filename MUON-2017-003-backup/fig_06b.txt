Invariant mass distribution for muon pairs reconstructed using the Medium working point in
simulated Z → µµ (a) and Z'→ µµ (b) events, in the mass range near the resonance mass. The same
scenarios as in Figure 1 are shown.

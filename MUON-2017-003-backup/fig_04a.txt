Relative resolution on q/p_{T} for Medium muons measured using simulated single muons
for four different pseudorapidity intervals. The resolutions are given for the ITk stand-alone (blue), the
MS stand-alone (red), and the combined track reconstruction (black) as solid markers. Lines are drawn
between the points to guide the eye. In addition, the performance achieved with the Run-2 Inner Detector
(ID) stand-alone (open blue squares) and the combined MS-ID reconstruction (open green circles) are
superimposed for comparison.

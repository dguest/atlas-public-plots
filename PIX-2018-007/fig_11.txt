 Average rate of the SEU/SET bit flips  in pixel memory of FE-I4-B per fb-1 in
 the fill 7334 as a function of the chip number. On the left figure the 
Shift Register (SR) was set to “1” and 0->1 flips dominate due to the SET
 (glitches) on the LOAD line, while low rate1->0 flips are due to real memory
 SEU. On the right figure the Shift Register was set to “0” and 1->0  flips 
dominate. The values of the Shift Register are refreshed several times during 
LHC fill. The extrapolation of the measurement of SEU rate with 24 GeV protons
 on CERN PS is shown with blue line on the right plot. During CERN PS measurement
 the value of SR was not refreshed which may explain higher rate of bit flips
 due to SET contribution. There are different chips on slightly different
 locations on two figures, so part of the difference between two figures may
 come from the chips to chip process variations, tuning and particle flow
 differences.  

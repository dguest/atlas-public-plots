Fraction of pixel hits with high noise as a function
of pixel TDAC during empty bunches of the typical
LHC Fill 6343. Strong suppression of the noise for
TDAC > 15 proves that biggest fraction of the noise
happens after the SEU flip 0->1 of MSB of TDAC,
which sharply reduce the pixel threshold. Low
values of TDAC corresponds to high thresholds.
TDAC values for each pixel are taken from initial
pixel configuration. In this fill the pixels with more
than 200 hits are defined as noisy.

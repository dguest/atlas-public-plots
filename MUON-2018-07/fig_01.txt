Muon reconstruction efficiency for the Medium identification algorithm measured in $Z\to\mu\mu$
 events as a function of the pT of the muon in the region $0.1 < |\eta| < 2.5$. The prediction by the detector
simulation is depicted as open circles, while filled dots indicate the observation in collision data with statistical uncertainties. 
The bottom panel shows the ratio between expected and observed efficiencies, the
efficiency scale factor. The shaded areas in the bottom panel show the statistical uncertainty (blue) and the quadratic sum of statistical and systematic uncertainties (orange). The results are 
based on the full $pp$ collision data collected in 2018 at 13 TeV.

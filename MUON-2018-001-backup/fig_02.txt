Muon reconstruction efficiency for the Medium  identification algorithm, as defined in Eur.Phys.J.C (2016) 76:292, measured in $Z\to \mu\mu$ events as a
 function of the $p_T$ of the muon in the region $0.1<|\eta|<2.5$
The prediction by the detector simulation is depicted as open circles, while filled dots indicate the observation in collision data with statistical errors. 
The bottom panel shows the ratio between expected and observed efficiencies, the efficiency scale factor. The errors in the bottom panel  show the quadratic sum of statistical and systematic uncertainty. 
The results are based on 4.0 fb$^{−1}$ of data collected in 2018.

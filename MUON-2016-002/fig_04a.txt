Sagitta alignment overall performance estimates
$\sigma_\mathrm{ali}$ for the muon spectrometer, determined from the
compatibility of the fluctuations of the per-tower sagitta bias
measurements $\mu_0$ ($\sigma_\mathrm{ali}(\mu_0)$), $\mu_\theta$
($\sigma_\mathrm{ali}(\mu_\theta)$) and $\mu_\phi$
($\sigma_\mathrm{ali}(\mu_\phi)$) with their statistical error.  The
quadratic sum of the three ($\sigma_\mathrm{ali}(\mathrm{total})$) is also
given, measuring the overall goodness of the alignment.  Regions of the
muon spectrometer are differentiated: bulk barrel large or small towers (BA
large, BA small), bulk end-cap large or small towers (EC large, EC small),
forward end-cap CSC large or small towers (CS large, CS small), end-cap
towers composed of EEL, EES or BEE (EE large, EE small, BEE).
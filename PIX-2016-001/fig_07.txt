The average truncation flag rate, the average number of truncation error flags per module per event, for Pixel B-Layer as a function of the average number of interactions per bunch crossing, in luminosity block units (one luminosity block corresponds approximately to one minute of data-taking).
The truncation error flag means the error bit flagged when the hit information is lost potentially.
The black (red) point shows the error rate with the time over threshold tuning of $30$ ($18$) for injected charge of $20,000$ electrons and level-1 latency setting of $255$ ($150$) bunch crossing.
The error rate reduces by a factor of $0.3$ with the latter configuration.

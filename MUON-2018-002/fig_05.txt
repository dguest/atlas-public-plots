Selection efficiencies for different muon isolation requirements measured as a function of the pileup.
The measurement is done with a tag-and-probe analysis of $Z\to\mu\mu$ in data events collected in 2017 and in simulation, for muons with $p_{T} > 10$ GeV. 
The errors on the efficiency are statistical, and no background subtraction has been used.
The FixedCutHighMuTight selection has been optimized for high pile-up environment using a better association of tracks in the isolation cone to the primary interaction vertex.

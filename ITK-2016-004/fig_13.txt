The maximum channel occupancy for a sensor in the ITk strip end-cap in a ttbar, $<\mu>$ = 200 event for 50,000 events.

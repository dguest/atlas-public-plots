The average hit density in the ITk strip barrel as a function of z, calculated using 1,000 minimum bias $<\mu>$ = 200 events and averaged over $\phi$.

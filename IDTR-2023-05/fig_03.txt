Tracking efficiency for single muons at $p_\mathrm{T} = 2$~GeV without pileup,
as a function of true pseudo-rapidity $\eta$ using the ITk Layout 03-00-00. The
efficiency is defined as the number truth particles with at least one
reconstructed track matched to them, divided by the total number of truth
particles.  A track is considered matched if the matching probability is over
50\% for a truth particle. A comparison of the ITk reconstruction with the Run
3 detector and reconstruction is shown.  Efficiencies agree at the sub-percent
level, while the increased $\eta$ coverage of ITk can be observed.

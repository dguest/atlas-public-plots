Efficiency of the seeding as a function of the pseudo-rapidity $\eta$ of the
associated truth particle, in $t\bar{t}$ events at $\langle\mu\rangle~=~200$
with a center of mass energy of $\sqrt{s}~=~14$ GeV, using the ITk Layout
03-00-00.  Seeds are built from triplets of space-points that are compatible
with a trajectory from the interaction point. Efficiency is defined as the
ratio between the number of truth particles with at least one reconstructed
seed matched it and all selected truth particles. The particles considered must
satisfy $p_\mathrm{T} >$ 1 GeV and $|\eta|< $ 4.0, and be produced by the
primary interactions. A seed is matched to a truth particle if at least 50\% of
the seed measurements belongs to the truth particle. The independent pixel-only
and strip-only seeding efficiencies are shown, in addition to the combined
seeding efficiency.

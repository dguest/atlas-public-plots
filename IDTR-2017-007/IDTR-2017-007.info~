<TITLE>
Average Number of Tracks against $\mu$ for Data 2017
<DATE>
29/11/17
<INTRO>
The average number of charged particle tracks that pass a preselection of $𝑝_𝑇 > $1 GeV and $|\eta|$ < 2.5 reconstructed per event in the ATLAS Inner Detector as a function of $<\mu >_{𝑏𝑢𝑛𝑐ℎ}$, for the Loose and TightPrimary track selections is shown. The track selections, Loose and TightPrimary are defined in <a href="https://cds.cern.ch/record/2110140/files/ATL-PHYS-PUB-2015-051.pdf">ATL-PHYS-PUB-2015-051</a>. $<\mu>_{𝑏𝑢𝑛𝑐ℎ}$  is the estimated mean number of inelastic interactions per bunch crossing, averaged over all colliding bunches. $\mu$ refers to the Poisson mean per bunch, and the brackets $<  >$ refer to the average over colliding bunches. The $\pm$ 4\%$ systematic variation on $<\mu>_{𝑏𝑢𝑛𝑐ℎ}$  represents a rough, preliminary estimate of the impact of present uncertainties associated with the absolute scale and the residual non-linearities of the luminosity measurement.
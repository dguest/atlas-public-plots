Stability of temperature of IBL modules of one run (270953) for (a) a stably operated stave 
(Stave02), and (b) a particular stave (Stave11) in which the low voltage power supply was interlocked in the 
middle for module group M4C, and the consequent drop of the temperature due to the power loss is seen. A 
stave is composed of twelve double-chip planar sensor modules in the centre region, and eight single-chip 3D 
sensor modules on the extremities. The colour scale shows the number of luminosity blocks (atomic unit of 
ATLAS data of the run) which have the temperature record in the corresponding temperature bin. Note that 
one temperature sensor monitors every four front-end chips (eight sensors in total for a stave). For (b), the 
temperature drop at M4C is partially conducted to the adjacent modules of M3C as well.

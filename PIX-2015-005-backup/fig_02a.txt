The trend of average of low voltage applied to IBL modules of one run (270953) 
for (a) a stably operated stave (Stave02), and (b) a particular stave (Stave11) in which the 
low voltage power supply was interlocked in the middle for module C7 and C8. For each 
stave there are eight independent low voltage supply lines, and the bullets in the plot show 
the average of the supplied voltage. The grey band represents the RMS values of the 
voltage of eight supply lines.

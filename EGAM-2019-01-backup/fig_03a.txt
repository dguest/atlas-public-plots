Examples of the energy scale extrapolation from high pile-up to low pile-up in the barrel (a) and end-cap (b).
The blue points show the energy scale factors $\alpha$ for the high pile-up dataset as a function of $\left\langle\mu\right\rangle$
(average number of proton--proton interactions per bunch crossing), the black lines show the extrapolation to $\left\langle\mu\right\rangle \sim$ 2
using a linear function and 5 intervals of $\left\langle\mu\right\rangle$, the band represents the uncertainty in the extrapolation.
The extrapolation results are compared to the energy scale factors extracted from the low pile-up dataset,
represented by the red point.

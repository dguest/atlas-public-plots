Energy scale factor $\alpha$ (a) and additional constant term c (b) for energy resolution from $Z \to ee$ events
as a function of $\eta$, applied to match data to MC. The differences (up to $\pm$ 0.5%) between the energy
scale factors for different years are mainly related to small changes in the detector conditions (LAr temperature) as well as
the change of luminosity. The additional constant term c depends on the level of pile-up. This is due
to an overestimation of the pile-up noise in the MC.

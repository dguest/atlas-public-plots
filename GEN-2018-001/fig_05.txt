Fraction of women in leadership positions and on selected committees. The horizontal dashed (solid) line shows the average fraction of women (aged over 35 years) in ATLAS of 20% (15%). 

The data are shown per term, i.e. if the same person held the responsibility for two terms there are two entries. 

For Institution Team Leaders the data shown are for July 2018.

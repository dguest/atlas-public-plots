The ratio &lt;p<sub>T</sub><sup>jet</sup>/p<sub>T</sub><sup>ref</sup>&gt; for anti-k<sub>t</sub> R=0.4 jets calibrated with the EM+JES (in-situ) scheme <a href="http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2015-015/">[1]</a> as a function of the calibrated jet &eta;, for both data and MC simulation. p<sub>T</sub><sup>ref</sup>=p<sub>T</sub><sup>&gamma;</sup> x |cos(&Delta;&phi;)| corresponds to the projection of the photon transverse momentum on the leading jet axis. The jets are selected in the central region of the hadronic calorimeter |&eta;<sup>lead jet</sup>| &lt; 0.8  and  pileup is suppressed through a cut on the jet-to-vertex tagger, or JVT, a multivariate combination of primary vertex and track-based variables <a href="http://arxiv.org/abs/1510.03823">[2]</a>. The photon is selected with |&eta;<sup>&gamma;</sup>| &lt; 1.37 and p<sub>T</sub><sup>ref</sup> &gt; 40 GeV.
The photon and the leading jet are required to be back-to-back by applying selection criteria to the azimutal angle between them (&Delta;&phi; &gt; 2.8) and event with  p<sub>T</sub><sup>second jet</sup> &gt; 0.1 x p<sub>T</sub><sup>ref</sup> are rejected. Only statistical uncertainties are shown. The transverse momentum response of forward jets has been corrected based on a dijet study using 50 ns early data, where central jet response is used a reference. The A14 tune and NNPDF23LO PDF has been used for Pythia8 sample, and the Sherpa default tune and CT10 PDF has been used for Sherpa 2.1 sample.

<br>

[1] <a href="http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2015-015/">ATL-PHYS-PUB-2015-015</a>

<br>

[2] <a href="http://arxiv.org/abs/1510.03823">arXiv:1510.03823 [hep-ex]</a>

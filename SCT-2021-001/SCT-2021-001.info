<TITLE>
THE PERFORMANCE AND OPERATIONAL EXPERIENCE OF THE SCT DURING LHC RUN 2 : Plots to present in as poster at TIPP 21
<DATE>
19-05-2021
<INTRO>
The performance of ATLAS Semiconductor Tracker (SCT) in Run-2 at the Large Hadron Collider (LHC) has been reviewed during the current long shutdown. The LHC successfully completed its Run-2 operation (2015-2018) with a total integrated delivered luminosity of 156 fb−1 at a centre-of-mass pp collision energy of 13 TeV. The LHC’s high performance provides a good opportunity for physics analysis. It came with high instantaneous luminosity and pileup conditions that were far in excess of what the SCT was originally designed to meet. The first significant effects of radiation damage in the SCT were observed during Run-2. This talk will summarise the operational experience and performance of the SCT during Run-2, with a focus on the impact and mitigation of radiation damage effects.

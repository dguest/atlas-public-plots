Table 1
This table displays a snapshot in May 2016 of the disabled SCT elements. These numbers are not static but evolve with time.
Modules contain 1536 strips and are disabled if inoperative due to cooling, Low Voltage, High Voltage or readout problems. 
The disabled chip count excludes chips from disabled modules. Chips contain 128 strips. Most of the disabled chips in the barrel modules are due to the application of optical redundancy in the readout, which normally requires the loss of one of the twelve module chips. The remainder are due to readout defects or excessive noise.
The disabled strip count does not include strips lost due to disabled chips or disabled modules. Disabled strips are generally isolated and spread over the entire SCT.
The active fraction refers to the total number of strips active for tracking purposes.


Table 2
This table provides an overview of why modules are disabled in the SCT configuration in May 2016. These numbers are not static but may evolve with time.

Cooling: All 13 modules are on one defective cooling loop, which has been off since before the start of LHC operations.

HV or HV: The module has no HV connection, or an HV short giving rise to high current, or has a fault with the LV powering. Faults due to the powering infrastructure (HV or LV power supplies) are excluded so the fault is either on the module itself or on the routing of power from the power supply to the module.

Readout: Unable to read out the data from the module due to a DAQ configuration or optical issue

Table 3
This table lists the number of disabled optical links between the readout system and the detector. TX refers to the transmission of trigger and commands to the module, so there is one TX link per module (4088 links in total). RX refers to the transmission of hit data from the module to the off-detector readout; there are two RX links per module, one for each module side (8176 in total).
The disabling of an optical link does not imply the loss of that channel. In the case of TX, redundancy is applied to source the trigger and commands from a neighboring channel. In the case of RX, redundancy is applied to read out both sides of the module through the single working link.


Per event distribution of the number of reconstructed tracks with $p_\mathrm{T}>100$ MeV from a reconstructed primary vertex, with a loose track 
selection applied, for proton-lead collisions at $\sqrt{s_{\mathrm{NN}}}=8.16$ TeV.  The events are triggered by the presence of two muons, each 
with $p_{\mathrm{T}(\mu)}> 4$ GeV.  A clear contribution is observed at exactly two tracks, indicative of exclusive pair production from 
ultraperipheral interactions, producing dileptons in the interaction of two photons.


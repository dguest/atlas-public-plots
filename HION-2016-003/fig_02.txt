Dielectron invariant mass distribution centered around the Z boson mass.  Electrons used may be measured 
by the tracking and calorimeters at mid-rapidity ($|\eta|<1.37$ or $1.52<|\eta|<2.47$) or the forward 
calorimeters ($|\eta|>2.5$).  From pairs made with two mid-rapidity electrons, opposite and same-sign 
distributions are shown.  Candidate electrons are required to have $p_\mathrm{T}>20$ GeV and pass a loose 
identification selection.  Events 
are selected with an unprescaled high-level electron trigger.  Uncertainties shown are statistical only.


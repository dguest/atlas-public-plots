The difference in decay length for corresponding tau leptons for standard and quasi-stable particle simulations.
In standard ATLAS simulation, the Geant4 feature of being able to provide pre-defined decays for particles is not used, while this is a key feature of quasi-stable particle simulation. In the case of quasi-stable particle simulation interactions with the material, a reduction in  the decay length is naively expected, while a symmetric broadening is observed. This is being followed up with Geant4 experts.
The simulation is done without the magnetic field in the inner detector and without any beam spot variations in order to better isolate other effects on the decay length.
The sample containing Drell-Yan production of $\tau$ lepton pairs with m$_{\tau\tau}>5$~TeV was used.

Muon reconstruction efficiencies for the Medium identification algorithm 
measured in $Z\rightarrow \mu\mu$ events as a function of the muon 
pseudorapidity for muons with $p_T>10$ GeV, in the full $|\eta|$ range (a) and for $|\eta|>0.1$ (b). 
The prediction by the detector simulation is depicted as open circles, while filled dots indicate the observation in collision data with statistical errors. 
The bottom panel shows the ratio between expected and observed efficiencies, the efficiency scale factor. The errors 
in the bottom panel show the quadratic sum of statistical and systematic uncertainty. The results are 
based on 15.8 fb$^{-1}$ of data collected in 2018.

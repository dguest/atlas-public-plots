Energy response as a function of <$\mu$> in 2017 data, normalised to the average over the year.
The energy response is probed using the peak position of the electron pair invariant mass peak
in $Z \to e^+e^-$ events and <$\mu$> is defined as the average number of $pp$ interactions per bunch crossing.
The error bars include statistical uncertainties only. The global stability is at the 0.05%
level. An improved BCID-dependent pileup correction is applied to the data
(https://twiki.cern.ch/twiki/bin/view/AtlasPublic/LArCaloPublicResults2015#LAr_Pulse_Shape_Plots_and_Impact).
The small increase of energy with <$\mu$> observed in data is consistent with the MC expectations and
is related to the new dynamical clustering used for the energy measurement
(https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PLOTS/EGAM-2017-001/index.html).

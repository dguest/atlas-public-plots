Transverse momentum distribution of muons coming from $Z\rightarrow \mu\mu$ events. 
  The oppositely charged, combined muon pairs are required to have $m_{\mu\mu}\in[75,105]$ and both muons have transverse momentum above 20 GeV. 
  Collision events are selected by an event filter trigger with a 14 GeV threshold. 
  The points show the data, corresponding to 6.6 pb$^{-1}$ of integrated luminosity; the filled histograms show the simulation with momentum and efficiency corrections applied. 
  The background was obtained from simulation and added to the signal sample according to their expected cross sections. 
  The sum of background and signal MC is normalized to the data. 

Dimuon invariant mass distribution for oppositely charged, combined muon pairs with transverse momentum above 20 GeV. 

  Collision events are selected by an event filter trigger with a 14 GeV threshold.

  The points show the data, corresponding to 6.6 pb$^{-1}$ of integrated luminosity; the filled histograms show the simulation with momentum and efficiency corrections applied. 

  The background was obtained from simulation and added to the signal sample according to their expected cross sections. 

  The MC is normalized to the number of $Z$ candidates observed in data in the mass window [75,105] GeV. 


Comparison of the radiative Z boson photon isolation (WP
FixedCutTight) efficiencies as function of <mu> for reconstructed
unconverted photons (a) and converted photons (b), for pT range
from 20 up to 40 GeV, for the entire pseudorapidity region. Solid
black dots represent data16, open red dots MC sample. The bottom
panel shows the ratio of the data results to the MC predictions. Only
the statistical uncertainties are shown.

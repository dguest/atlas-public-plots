Per-tower sagitta biases $\mu_\theta$ measured in the August 2015 toroid-off run, where for each tower of the
muon spectrometer, the alignment bias on the sagitta is modelled as $\mu_0
+ \frac{\theta-<\theta>}{\mathrm{RMS}(\theta)}\mu_\theta +
\frac{\phi-<\phi>}{\mathrm{RMS}(\phi)}\mu_\phi$, with $\theta$ and $\phi$
the polar and azimuthal angles, and average and $\mathrm{RMS}$ taken over
the tower track sample. The sagitta is defined here for tracks crossing
three chambers in a single sector, as the distance between the middle
chamber measurement and the line connecting inner and outer chambers
measurements, projected in the precision plane. The yellow stripes indicate
the range $\pm 0.1 mm$. Regions of the muon spectrometer are
differentiated: bulk barrel large or small towers (BA large, BA small),
bulk end-cap large or small towers (EC large, EC small), forward end-cap
CSC large or small towers (CS large, CS small), end-cap towers composed of
EEL, EES or BEE (EE large, EE small, BEE).

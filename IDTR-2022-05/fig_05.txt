The distributions of the number of SCT hits, in a dijet fake-enriched control region.
The distributions are obtained using Run 2 LHC data and MC simulation,
processed with the new ATLAS software release that will be used for Run 3 data taking.
The MC distributions are also plotted separately for real (down-triangles) and fake (squares) tracks.
Last bin includes overflow. 
The fit results are also shown. The fit procedure is discussed in 
<a href="https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2017-016/">ATL-PHYS-PUB-2017-016</a>.
Tracks are selected using the "loose" track quality criterion
<a href="https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2015-018/">ATL-PHYS-PUB-2015-018</a>,
<a href="https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2021-012/">ATL-PHYS-PUB-2021-012</a>. 
Only the statistical uncertainties are shown. The fake tracks are defined using the standard ATLAS truth-matching requirements, 
P$_{\mathrm{match}} < 0.5$
<a href="https://cds.cern.ch/record/2110140/files/ATL-PHYS-PUB-2015-051.pdf/">ATL-PHYS-PUB-2015-051</a>.	

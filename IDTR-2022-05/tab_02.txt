Fraction of fake tracks at pre-selection level, $f^{\mathrm{fakeTrk}}_{\mathrm{pre-Sel}}$.
The method used to extrapolate the results from the dijet fake enriched
control region (CR) to the dijet pre-selection level is detailed in 
<a href="https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2017-016/">ATL-PHYS-PUB-2017-016</a>.
$f^{\mathrm{fakeTrk}}_{\mathrm{CR}}$ is taken from the fit of the selected variables distributions. 
The fake tracks are defined using the standard ATLAS truth-matching requirements,
P$_{\operatorname{match}} < 0.5$
<a href="https://cds.cern.ch/record/2110140/files/ATL-PHYS-PUB-2015-051.pdf/">ATL-PHYS-PUB-2015-051</a>.
$n_{\mathrm{dof}}$ stands for the number of degrees of freedom.

The radial vertex position distribution for hadronic interaction candidates reconstructed from multiple tracks 
with $>5\sigma$ transverse impact parameter significance. The distribution exhibits peaks consistent with
hadronic interactions occurring within the new ATLAS beam pipe and the Insertable B-Layer (IBL) in addition to 
the original three Pixel layers. The hadronic interaction candidates are required to be reconstructed within 
$r>20~{\rm mm}$ and $|z|<300~{\rm mm}$. Due to impact parameter cuts, the efficiency depends on the radius. 
No attempt has been made to correct for this.

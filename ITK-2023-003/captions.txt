fig_01: 
Plots show the fluence map obtained by the Al foils raw measurement,  refined with the local efficiency map. The whitearea represents  the fiducial region where  the beam was impinging. The area is constrained by the scintillators trigger and damages  on the specific modules under test.Average  fluence in the fiducial area is 2.4&middot10<sup>16</sup> n<sub>eq</sub> / cm<sup>2</sup> for sensor #23.

fig_02: 
Plots show the fluence map obtained by the Al foils raw measurement,  refined with the local efficiency map. The whitearea represents  the fiducial region where  the beam was impinging. The area is constrained by the scintillators trigger and damages  on the specific modules under test.Average  fluence in the fiducial area is 2.3&middot10<sup>16</sup> n<sub>eq</sub> / cm<sup>2</sup> for sensor #24.

fig_03: 
Average efficiency as a function of sensor bias voltage after uniform irradiation to 1&middot10<sup>16</sup> n<sub>eq</sub> / cm<sup>2</sup> (KIT –23 MeV protons) measured with devices perpendicular to the beam (0 degree) and tilted by 15 degree with respect to the beam axis. The efficiency and the number of masked pixels are measured over the entire sensor area hit by the test beam, over which the fluence is uniform.

fig_04: 
Average efficiency (right axis) and fraction of masked pixels (left axis) as a function of sensor bias voltage for different received fluences measured with devices perpendicular to the beam (0 degree) and tilted by 15 degree. Measurements are done with modules that received not uniform irradiation up to2.4&middot10<sup>16</sup> n<sub>eq</sub> / cm<sup>2</sup> at IRRAD.Efficiencies and the fraction of masked pixels were calculated in the same fiducial area, where the received fluence is calculated from the reconstructed fluence map

fig_05: 
Average efficiency as a function of sensor bias voltage after uniform irradiation close to 1&middot10<sup>16</sup> n<sub>eq</sub> / cm<sup>2</sup> measured with devices perpendicular to the beam (&#977 &#61 0 degree) and tilted by 15 degree(&#977 &#61 15 degree). The efficiency is measured over the entire sensor area hit by the test beam, over which the fluence is uniform.Sensor #3 has 50x50 &#x03bcm&sup2 pixel cell, while #29 has pixel pitch 25x100 &#x03bcm&sup2. Both sensors are produced by Fondazione Bruno Kessler.Sensor #3 has been irradiated in Bonn with a 12 MeV proton beam in June 2022. Sensor #29 has been irradiated in KIT with a 23 MeV proton beam in August 2023

IBL local-x correction in the transverse plane averaged over all 14 IBL staves for 2015 data (red open squares), and for 2016 data using different Tset  (+15℃, solid blue circles; +5℃, solid green triangles). 
No error bars associated with data are shown. 
The IBL distortion was constant during the duration of all three LHC fills. 
Here, only the correction due to the IBL distortion is shown. 
The baseline, which describes the overall translation of the whole stave, is subtracted using a parabolic fit function parameterizing the IBL temperature dependent distortion. 
In this fit the baseline is corrected so that the fitted curve is zero at the edges of the staves ±366.5 mm.

The plot shows the SCT cluster size (in strips) plotted as a function of the track incidence angle for the 4 layers in the SCT barrel, for modules assembled from sensors with <111> crystal orientation. The incidence angle is measured in degrees. We note that the minimum of the distribution is in each case displaced from the zero by the measured Lorentz angle. 
The data correspond to early 13TeV collisions in June 2015.


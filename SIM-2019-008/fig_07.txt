<p>Projections of the parametrized (blue) and optimized via evolutionary algorithm (red) shape histograms of a single pion of energy 2 TeV in the range 0.20 < |&eta;| < 0.25 along the distance axis <i>R</i>. Parametrized shape is represented by uniform 5 mm binning in the range 0 to 6270 mm. Optimized shape is represented by non-uniform binning (15 bins in the same range) with linear function used for interpolation inside each bin. 
Deviations between parametrized and optimized histograms for all <i>R</i> are limited by the following relation:</p>

<p>|&int;<sub>0</sub><sup> <i>R</i></sup> <i>H</i><sub>par</sub> - &int;<sub>0</sub><sup><i>R</i></sup><i>H</i><sub>opt</sub>| < 0.01 &int;<sub><i>R</i></sub><sup><i>R</i><sub>max</sub></sup> <i>H</i><sub>par</sub> + 0.0002 &int;<sub>0</sub><sup><i>R</i><sub>max</sub></sup><i>H</i><sub>par</sub>,</p>

<p> where <i>H</i><sub>par</sub> and <i>H</i><sub>opt</sub> are parametrized and optimized shapes respectively, <i>R</i><sub>max</sub> is upper bound of the shape range.</p>

<p>Optimized histogram uses 16-bit representation for bin edges over <i>R</i>-axis and 32-bit representation for the bin contents. Total size of 2D (<i>R</i>, &alpha;) template (8 &alpha; bins with 8-bit representation for edges) is 353 bytes. The size of 2D parametrized shape (with 16-bit <i>R</i> and 8-bit &alpha; edges representation) is 43 Kbytes.</p>
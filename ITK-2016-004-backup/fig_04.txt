The maximum channel occupancy for a sensor in the ITk strip barrel in a ttbar, $<\mu>$ = 200 event for 50,000 events.

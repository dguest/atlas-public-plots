The maximum number of hits in a sensor in the ITk strip barrel in a ttbar, $<\mu>$ = 200 event for 50,000 events.

The average pixel cluster density, as a function of z in the ITk pixel end-cap for the extended layout, calculated from 1,000 $<\mu>$ = 200 minimum bias events and averaged over $\phi$.

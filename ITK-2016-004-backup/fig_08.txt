The average cluster size as a function of R in the ITk strip end-cap, calculated from 1,000 $<\mu>$ = 200 minimum bias events and averaged over $\phi$.

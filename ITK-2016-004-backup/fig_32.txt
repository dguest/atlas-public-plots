The average hit density in the ITk strip detector in a 2D plane of R & z, calculated using 1,000 minimum bias $<\mu>$ = 200 events and averaged over $\phi$.

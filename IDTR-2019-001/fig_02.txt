Average values as a function of the reconstructed muon azimuthal angle $\phi$, 
of $\delta m = m_{fit} − m_{MC}$ divided by $m_{MC}$, 
where $m_{fit}$ is the peak of the $J/\psi \rightarrow \mu^+\mu^-$ invariant mass reconstructed in data 
and $m_{MC}$ is the peak of the invariant mass reconstructed in the Monte Carlo simulated samples, 
for data collected in 2016 and 2017. 
The two muons are required to have $\left|\eta\right| < 1.17$ which corresponds to the barrel region of the SCT. 
The formula used to fit the distribution is:
$\delta m(\phi)/m_{\small MC} = B~ +~A \cos(N\cdot(\phi−\phi_0))$ 

(left) Distribution of the pair rapidity ($Y_{\mu\mu}$) for events with two reconstructed tracks and two reconstructed muons, each of which have 
$p_{\mathrm{T}(\mu)}>5$ GeV, $|\eta(\mu)|<2.4$, pair $p_\mathrm{T,\mu\mu}<2$ GeV, and pass medium identification selection.  No efficiency 
corrections are applied.  The shape of the distribution is compared to that calculated with STARLIGHT 1.1, which incorporates realistic 
equivalent photon distributions from the proton and lead nucleus, and the leading order QED diagram for the $\gamma+\gamma \rightarrow \mu^{+} + 
\mu^{-}$ process.  (right) Correlation of the transverse momentum between the two selected muons, showing the strong correlation typical of 
back-to-back dimuon production.


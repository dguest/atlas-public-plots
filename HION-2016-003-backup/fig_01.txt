Uncorrected invariant mass distribution of oppositely charged muon pairs with $\left|\eta^{\mu}\right|<2.4$ 
in events firing a high-level trigger requiring two muons with $p_T > 4$ GeV (upper) and a high-level trigger 
requiring a muon with $p_T > 15$ GeV and which is seeded by a level-1 muon trigger with a threshold of 6 GeV 
(lower). Both muons in the pair must satisfy medium muon selection criteria. All luminosity collected during 
p+Pb run is used.

Run 2 Data  and Standalone Simulation, based on Geant4, of the Charge Collection Efficiency as a function of the integrated Luminosity 
for IBL modules with $|\eta|<0.2$. 
Simulation based on geant4 inputs,  Electric field maps produced with TCAD simulation based on the Chiochia radiation damage model
(Chiochia, V. and others, Nucl. Instr. and Meth A 568 (2006) 51–55 ), and accurate description of electrons and holes drift in
 the silicon detector. Data are corrected to account for ToT drift in the calibration process. 	
 For the simulation, the vertical bars include radiation damage parameter variations and the horizontal 
errors bars reflect uncertainty in the lumi-to-fluence conversion.
 
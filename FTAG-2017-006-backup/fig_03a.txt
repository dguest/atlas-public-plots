The distribution of the RNNIP impact parameter-based discriminant
applied to jets in a t<span style="text-decoration: overline">t</span>-dominated 
sample of events selected by requiring an opposite-sign e&mu; pair and 
at least two jets. The 2015+2016 data is shown by the points with error bars 
and the simulation by  the filled areas, divided into contributions from 
b (red), c (green) and light (purple) flavoured jets, and normalised
to the same number of jets as the data.

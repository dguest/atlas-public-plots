Fraction of jets that are b-tagged with the MV2c10 77% fixed-cut working point, in 2017 data events selected by requiring an opposite-sign e&mu; pair and at least two jets. 
The estimated jet flavour composition is 59% b, 2%  c and 38% light jets.
The fraction is shown as a function of &lt;&mu;&gt;, the average number of pp interactions per LHC bunch crossing.

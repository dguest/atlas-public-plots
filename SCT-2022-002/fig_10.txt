(left) Evolution of the relation between the HV current and the HV ($I$-$V$ curve) for one module in barrel layer 3, from June 2009 to Sept. 2022. 

(right) Distribution of $V_{FD}$ (Full Depletion Voltage) for 4 barrel layers obtained in Nov. 2011, Mar. 2017, Nov. 2018 and May 2022. $V_{FD}$ is estimated as the point of kink in the I-V curve. Initially the $V_{FD}$ thus obtained are not far from $V_{FD}$ measured by the sensor C-V curve. The $V_{FD}$ is low at early 2017 while at the  2018 end the it went up again, indicating sensor type inversion.



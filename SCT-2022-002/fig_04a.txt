Hit efficiency (a) and hit occupancy (b) as a function of threshold for SCT barrel layers 3 and 6 measured in threshold scan data taken during $pp$ collision runs in 2018 and 2022. Good performance is observed for thresholds up to 1.5 fC for both 2018 and 2022 data. The lower level of occupancy recorded in 2022 corresponds to the lower $<\mu>$ value.

